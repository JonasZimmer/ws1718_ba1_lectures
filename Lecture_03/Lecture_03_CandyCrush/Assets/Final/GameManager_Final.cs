﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// GAME MANAGER CLASS
/// This class controls the game state
/// 
/// BA1 Game Programming - Jonas Zimmer
/// Recreate a Candy Crush like game
/// </summary>
public class GameManager_Final : MonoBehaviour
{
    private Tile_Final[,] grid; //Two dimensional array of Tiles, representing the Grid
    public GameObject tilePrefab; //Prefab to instantiate one tile
    public RectTransform tileParent; //Reference to the rect transform that will be the instantiated tiles parent object
    public Text scoreText; //reference to the UI Text to display the score
    private int score; //score

    int width = 15; //width of the grid
    int height = 10; //height of the grid

    /// <summary>
    /// UNITY SYSTEM FUNCTION
    /// Initilizes the game. Creates the grid and randomly populates it.
    /// </summary>
    private void Start()
    {
        grid = new Tile_Final[width, height]; //Creates a new two dimensional array of tiles, representing the grid 
        score = 0;
        PopulateGrid();
    }

    /// <summary>
    /// Fills the empty grid with elements. Instantiates Tile prefabs 
    /// and grabs their Tile_Final component to reference it in the
    /// corresponding grid position: 
    /// (grid[x,y] = newGameObject.GetComponent<Tile_Final>())
    /// </summary>
    private void PopulateGrid()
    {
        //Iterates through the rows in the outer loop...
for (int y = 0; y < height; y++)
{
    //...and for each outer loop iteration once through the collumns
    for (int x = 0; x < width; x++)
    {
        GameObject newObject = (GameObject) GameObject.Instantiate(tilePrefab, tileParent); //Instantiates a new object...
        grid[x, y] = newObject.GetComponent<Tile_Final>(); //...grabs it Tile component and saves it in the grid at position x,y
        grid[x, y].Initialize(this, x, y); //Initializes the tile by calling its Initialize() function
        grid[x, y].SetTile(Random.Range(0, 8)); //Randomely sets a tiles value
    }
}
    }

    /// <summary>
    /// Checks how many matching neighbours a tile has
    /// </summary>
    /// <param name="x">the horizontal position of the tile to check</param>
    /// <param name="y">the vertical position of the tile to check</param>
    public void Check(int x, int y)
    {
        //Collecting matching tiles in a list
        List<Tile_Final> matchingHorizontalTiles = new List<Tile_Final>();
        
        //Check to the right
        int checkX = x + 1; //start with one element beside the clicked one
        while(true)
        {
            if (checkX >= width) break; //break the loop if we exceed the grids dimension
            if (grid[x, y].type == grid[checkX, y].type) matchingHorizontalTiles.Add(grid[checkX, y]); //if the tile has the same value as the matched one, add it to the list...
            else break; //...else break the loop
            checkX = checkX + 1; //Move the tile to check once to the right
        }

        //Check to the left
        checkX = x - 1; //start with one element beside the clicked one
        while (true)
        {
            if (checkX < 0) break; //break the loop if we exceed the grids dimension
            if (grid[x, y].type == grid[checkX, y].type) matchingHorizontalTiles.Add(grid[checkX, y]); //if the tile has the same value as the matched one, add it to the list...
            else break; //...else break the loop
            checkX = checkX-1; //Move the tile to check once to the left
        }

        //Collecting matching tiles in a list
        List<Tile_Final> matchingVerticalTiles = new List<Tile_Final>();

        //Check beneath tile
        int checkY = y + 1; //start with one element below the clicked one
        while (true)
        {
            if (checkY >= height) break; //break the loop if we exceed the grids dimension
            if (grid[x, y].type == grid[x, checkY].type) matchingVerticalTiles.Add(grid[x, checkY]); //if the tile has the same value as the matched one, add it to the list...
            else break; //...else break the loop
            checkY = checkY +1; //Move the tile to check down
        }

        //Check above tile
        checkY = y - 1; //start with one element above the clicked one
        while (true)
        {
            if (checkY < 0) break; //break the loop if we exceed the grids dimension
            if (grid[x, y].type == grid[x, checkY].type) matchingVerticalTiles.Add(grid[x, checkY]); //if the tile has the same value as the matched one, add it to the list...
            else break; //...else break the loop
            checkY = checkY- 1; //Move the tile to check up
        }

        //We have a match if either horizontal or vertical matching tiles are at least 3
        if (matchingVerticalTiles.Count >= 2 || matchingHorizontalTiles.Count >= 2)
        {
            Debug.Log("We've got a match!");
            int points = 20; //Base points

            grid[x,y].SetTile(Random.Range(0, 8)); //Sets a new value for the clicked tile

            foreach (Tile_Final t in matchingHorizontalTiles) //Iterates through all horizontal matching tiles...
            {
                t.SetTile(Random.Range(0, 8));//...sets a new value for them and...
                points = points + 5;//...adds some points
            }
            foreach (Tile_Final t in matchingVerticalTiles)//Iterates through all vertical matching tiles...
            {
                t.SetTile(Random.Range(0, 8));//...sets a new value for them and...
                points = points + 5;//...adds some points
            }
            Score(points);
        }
        else
        {
            Debug.Log("No matching tiles.");
        }
    }

    /// <summary>
    /// Function to Increase the Score and change the text
    /// </summary>
    /// <param name="amount"></param>
    private void Score(int amount)
    {
        score = score + amount;
        scoreText.text = "SCORE " + score.ToString("000");
    }
}
