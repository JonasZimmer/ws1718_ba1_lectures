﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// TILE CLASSE
/// Organizes a single tile
/// </summary>
public class Tile_Final : MonoBehaviour
{
    private RectTransform rTransform; //reference to the rect transform to change scaling
    private Image image; //reference to the Image component to change sprites
    private GameManager_Final gameManager; //reference to the game manager object to inform about clicks

    public float scaleRange; //the maximum scale change
    public Vector2 scale; //the current scaling
    private float xIdle, yIdle; //variables that store random values to determine the duration of a horizontal (xIdle) or vertical (yIdle) scale-cycle

    public int type; //current tile type (its value)
    public Sprite[] types; //all the types

    private int x, y; //coordinates of the tile

	/// <summary>
    /// Initializes a tile
    /// </summary>
    /// <param name="gm">a reference to the game manager</param>
    /// <param name="xPos">the horizontal coordinate of the tile</param>
    /// <param name="yPos">the vertical coordinate of the tile</param>
	public void Initialize(GameManager_Final gm, int xPos, int yPos)
    {
        rTransform = gameObject.GetComponent<RectTransform>(); //get a reference to the rect transform to scale the element
        image = gameObject.GetComponent<Image>(); //get a reference to the image component to change the sprites
        gameManager = gm; //sets the reference to the game manager to inform it about a click
        x = xPos; //saves the tiles horizontal coordinate
        y = yPos; //saces the tiles vertical coordinate
        gameObject.name = "Tile_" + x.ToString("00") + "_" + y.ToString("00"); //changes the tiles name so you can see its coordinates on one gaze
    }
	
	// Update is called once per frame
	void Update ()
    {
        /*
         * Scales the tile with different time modificators for 
         * horizontal and vertical scaling to avoid too much of
         * a pattern.
         * This is only a design choice!
         */
        scale = new Vector2(
            (1f - (scaleRange / 2f)) + (Mathf.Sin(Time.time * xIdle) * scaleRange),
            (1f - (scaleRange / 2f)) + (Mathf.Sin(Time.time * yIdle) * scaleRange));
        rTransform.localScale = scale; //Applies the scale
    }

    /// <summary>
    /// Sets a new type for the tile
    /// </summary>
    /// <param name="newType">the new type value</param>
    public void SetTile(int newType)
    {
        //Everytime the type change, we set new scaling factors
        xIdle = Random.Range(0.5f, 1.5f);
        yIdle = Random.Range(0.5f, 1.5f);

        type = newType;
        image.sprite = types[type]; //Changes the sprite based on the new type
    }

    /// <summary>
    /// Informs the game manager about a click on this tile
    /// </summary>
    public void OnClick()
    {
        gameManager.Check(x, y);
    }
}
