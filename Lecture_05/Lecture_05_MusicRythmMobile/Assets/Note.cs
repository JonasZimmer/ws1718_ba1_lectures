﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages a single note
/// </summary>
public class Note : MonoBehaviour
{
    private RectTransform rect;

    void Awake()
    {
        rect = gameObject.GetComponent<RectTransform>();
    }

    void Update ()
    {
        //move the note
        rect.anchoredPosition -=
            new Vector2(0, Time.deltaTime * ScrollingBackground.scrollSpeed);

        if (rect.anchoredPosition.y < -2200) //Deletes the note if it leaves the screen
        {
            Destroy();
        }
    }

    void Destroy()
    {
        GameObject.Destroy(this.gameObject);
    }
}
