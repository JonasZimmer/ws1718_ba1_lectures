﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages the button behaviour
/// </summary>
public class ButtonBehaviour : MonoBehaviour
{
    GameObject note;

    private void OnTriggerEnter2D(Collider2D other)
    {
        note = other.gameObject;
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        note = null;
    }

    // On Button Click, Destroy the note
    public void OnClick()
    {
        if (note != null)
            GameObject.Destroy(note);
    }
}
