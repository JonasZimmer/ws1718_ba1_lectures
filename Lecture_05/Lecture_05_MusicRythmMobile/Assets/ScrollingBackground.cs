﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// PARALLAX SCROLLING CLASS
/// This class moves an element at a specific speed and loops it 
/// to create the effect of an infinite background.
/// 
/// BA1 Game Programming - Jonas Zimmer
/// Recreate FLAPPY BIRD, the addictive surprising hit from 2013
/// </summary>
public class ScrollingBackground : MonoBehaviour
{
    public static float scrollSpeed = 600; //how far (in pixel) should these elements scroll per second
    private float size = 420; //the height of the sprite, hardcoded. Sprites must match this value. Bad coding behaviour! Its better to set this based on the actual used sprite, set in "Start()"
    private RectTransform rect; //reference to the RectTransform.

    /// <summary>
    /// UNITY SYSTEM METHOD
    /// Start is called at the creation of the class
    /// Gets RectTransform component
    /// </summary>
    void Start()
    {
        rect = gameObject.GetComponent<RectTransform>();
    }

    /// <summary>
    /// UNITY SYSTEM METHOD
    /// Updates the gameobjects position each frame
    /// </summary>
    void Update()
    {
        //Repeat will increase the value as long as value < size.
        //As soon as its >= size, it will "repeat" from 0.
        //Ie. value is 1022, increased by 5 -> 1027. Bigger than size (1024), so Repeat will handle it as 3.
        rect.anchoredPosition =
            new Vector3(0, -Mathf.Repeat(Time.time * scrollSpeed, size), 0);
    }
}
