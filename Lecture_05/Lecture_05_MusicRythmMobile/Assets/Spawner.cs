﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Spawning helper class
/// Marks the position to spawn the prefab
/// </summary>
public class Spawner : MonoBehaviour
{
    public GameObject prefab; // reference to the prefab
	
    public void Spawn()
    {
        GameObject.Instantiate(prefab, transform); //spawns prefab as child of this game object, using this position
    }
}
