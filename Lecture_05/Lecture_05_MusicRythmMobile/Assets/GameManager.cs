﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// GAME MANAGER CLASS
/// BA1 Game Programming - Jonas Zimmer
/// Mobile Music Rhythm Game
/// </summary>
public class GameManager : MonoBehaviour
{
    private AudioSource audiosource; //reference to the audio source
    public Song song; //reference to the song to play
    public Spawner G, R, Y, B, O; //references to the spawning locations for the different types of notes

    /// <summary>
    /// Custom class to store every information about one song
    /// </summary>
    [Serializable]
    public class Song
    {
        public string name; //name of the song
        public AudioClip song; //soundfile
        public float delay; //delay in seconds before the song should start (so when the first note hits the buttons)
        public float bpm; //beats per minute
        public float playLength; //how long should the song be played?
        [HideInInspector]
        public List<Beat> beats; //List of beats. They're hidden in inspector because they're displayed using a custom editor
    }

    /// <summary>
    /// Custom class to store a single beats informations
    /// </summary>
    [Serializable]
    public class Beat
    {
        public float time; //time actually refers to the number of beats
        public bool green, red, yellow, blue, orange; //defines which notes to play at that beat
    }

    /// <summary>
    /// Sets everything up
    /// </summary>
    private void Start()
    {
        audiosource = gameObject.GetComponent<AudioSource>();
        Play(song); //starts the song directly
    }


    private bool isPlaying; //currently a song is playing
    private bool songStarted; //the delay time of the song is over and it started playing

    //These are temporary variables, storing the information of the currently active song.
    private List<Beat> beatsToPlay; //a copy of the active songs beat list. If a beat got played, it'll be deleted from this list
    private float timeSinceStart; //counts the songs playtime
    private float timeToWaitBeforeStartingSong; //copy the Song.delay here
    private float timeBetweenBeats; //Calculate basd on Song.bpm
    private float timeToPlayFor; //Copy Song.playLength

    /// <summary>
    /// This function sets the boolean, calculates the bps and
    /// copies the relevant informations from the Song to the
    /// temporary variables.
    /// </summary>
    /// <param name="songToPlay">reference to the song to be played</param>
    private void Play(Song songToPlay)
    {
        isPlaying = true;
        songStarted = false;
        beatsToPlay = songToPlay.beats;
        timeBetweenBeats = 60f / songToPlay.bpm;
        timeToWaitBeforeStartingSong = songToPlay.delay;
        timeToPlayFor = songToPlay.playLength + songToPlay.delay;
        audiosource.clip = songToPlay.song;

        timeSinceStart = 0;
    }

    /// <summary>
    /// Stops the song
    /// </summary>
    private void Stop()
    {
        audiosource.Stop();
        isPlaying = false;
        beatsToPlay.Clear();
    }


    private void Update()
    {
        if (isPlaying == true)
        {
            timeSinceStart += Time.deltaTime;
            //Iterates through the list of beats to play...
            foreach (Beat b in beatsToPlay) 
            {
                //...and checks wether its time to Create a beat
                if (b.time * timeBetweenBeats <= timeSinceStart)
                {
                    SpawnBeat(b);
                    beatsToPlay.Remove(b);
                    break; //if theres a beat to be played, it will stop iterating further
                }
            }
            if (timeSinceStart >= timeToPlayFor)
                Stop();
            if (timeSinceStart >= timeToWaitBeforeStartingSong && songStarted == false) //starts the song if time > delay
            {
                audiosource.Play();
                songStarted = true;
            }
        }
    }

    /// <summary>
    /// Spawns the notes based on the beat
    /// </summary>
    /// <param name="b"></param>
    private void SpawnBeat(Beat b)
    {
        if (b.green) G.Spawn();
        if (b.red) R.Spawn();
        if (b.yellow) Y.Spawn();
        if (b.blue) B.Spawn();
        if (b.orange) O.Spawn();
    }
}




