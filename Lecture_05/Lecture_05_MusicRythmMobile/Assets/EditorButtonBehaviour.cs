﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Mimics the Touch Button Behaviour in Editor
/// </summary>
public class EditorButtonBehaviour : MonoBehaviour
{
    public KeyCode key;
    private Button button;
    private Image image;
    public Sprite normal, pressed;

	void Start ()
    {
        button = gameObject.GetComponent<Button>();
        image = gameObject.GetComponent<Image>();
	}

	void Update ()
    {
		if (Input.GetKeyDown(key))
        {
            image.sprite = pressed;
            button.onClick.Invoke();
        }
        if (Input.GetKeyUp(key))
        {
            image.sprite = normal;
        }
    }
}
