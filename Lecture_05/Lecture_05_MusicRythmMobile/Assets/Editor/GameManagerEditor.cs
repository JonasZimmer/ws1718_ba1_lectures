﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(GameManager))]
public class GameManagerEditor : Editor
{
    private ReorderableList reorderableList;

    private GameManager gameManager
    {
        get
        {
            return target as GameManager;
        }
    }

    private void OnEnable()
    {
        reorderableList = new ReorderableList(gameManager.song.beats, typeof(GameManager.Beat), true, true, true, true);

        // This could be used aswell, but I only advise this your class inherrits from UnityEngine.Object or has a CustomPropertyDrawer
        // Since you'll find your item using: serializedObject.FindProperty("list").GetArrayElementAtIndex(index).objectReferenceValue
        // which is a UnityEngine.Object
        // reorderableList = new ReorderableList(serializedObject, serializedObject.FindProperty("list"), true, true, true, true);

        // Add listeners to draw events
        reorderableList.drawHeaderCallback += DrawHeader;
        reorderableList.drawElementCallback += DrawElement;

        reorderableList.onAddCallback += AddItem;
        reorderableList.onRemoveCallback += RemoveItem;
    }

    private void OnDisable()
    {
        // Make sure we don't get memory leaks etc.
        reorderableList.drawHeaderCallback -= DrawHeader;
        reorderableList.drawElementCallback -= DrawElement;

        reorderableList.onAddCallback -= AddItem;
        reorderableList.onRemoveCallback -= RemoveItem;
    }

    /// <summary>
    /// Draws the header of the list
    /// </summary>
    /// <param name="rect"></param>
    private void DrawHeader(Rect rect)
    {
        GUI.Label(new Rect(rect.x, rect.y, rect.width - (6 * 18), rect.height), "Beat");
        GUI.Label(new Rect(rect.width - (5 * 18), rect.y, 18, rect.height), "G");
        GUI.Label(new Rect(rect.width - (4 * 18), rect.y, 18, rect.height), "R");
        GUI.Label(new Rect(rect.width - (3 * 18), rect.y, 18, rect.height), "Y");
        GUI.Label(new Rect(rect.width - (2 * 18), rect.y, 18, rect.height), "B");
        GUI.Label(new Rect(rect.width - (1 * 18), rect.y, 18, rect.height), "O");
    }

    /// <summary>
    /// Draws one element of the list (ListItemExample)
    /// </summary>
    /// <param name="rect"></param>
    /// <param name="index"></param>
    /// <param name="active"></param>
    /// <param name="focused"></param>
    private void DrawElement(Rect rect, int index, bool active, bool focused)
    {
        GameManager.Beat item = gameManager.song.beats[index];

        EditorGUI.BeginChangeCheck();
        item.time   = EditorGUI.FloatField(new Rect(rect.x, rect.y, rect.width - (6 * 18), rect.height), item.time);
        item.green  = EditorGUI.Toggle(new Rect(rect.width - (4 * 18), rect.y, 18, rect.height), item.green);
        item.red    = EditorGUI.Toggle(new Rect(rect.width - (3 * 18), rect.y, 18, rect.height), item.red);
        item.yellow = EditorGUI.Toggle(new Rect(rect.width - (2 * 18), rect.y, 18, rect.height), item.yellow);
        item.blue   = EditorGUI.Toggle(new Rect(rect.width - (1 * 18), rect.y, 18, rect.height), item.blue);
        item.orange = EditorGUI.Toggle(new Rect(rect.width - (0 * 18), rect.y, 18, rect.height), item.orange);

        if (EditorGUI.EndChangeCheck())
        {
            EditorUtility.SetDirty(target);
        }

        // If you are using a custom PropertyDrawer, this is probably better
        // EditorGUI.PropertyField(rect, serializedObject.FindProperty("list").GetArrayElementAtIndex(index));
        // Although it is probably smart to cach the list as a private variable ;)
    }

    private void AddItem(ReorderableList list)
    {
        gameManager.song.beats.Add(new GameManager.Beat());

        EditorUtility.SetDirty(target);
    }

    private void RemoveItem(ReorderableList list)
    {
        gameManager.song.beats.RemoveAt(list.index);

        EditorUtility.SetDirty(target);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        // Actually draw the list in the inspector
        reorderableList.DoLayoutList();
    }
}