﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// MAIN CHARACTER CLASS
/// This class controls the playable character, Son Goku
/// 
/// BA1 Game Programming - Jonas Zimmer
/// Recreate FLAPPY BIRD, the addictive surprising hit from 2013
/// </summary>
public class SonGokuController_Final : MonoBehaviour
{
    public float upForce; //The strength the player is accelerated if he jumps

    private bool isDead; //if the player is dead, the input should be ignored.
    private Rigidbody2D rbody; //Reference to the Rigidbody on this gameobject


    private void Start()
    {
        isDead = false;
        rbody = gameObject.GetComponent<Rigidbody2D>(); //Checks if there's a Rigidbody2D component attached to the same game object the script is attached to.
    }

    private void Update ()
    {
		if (isDead == false) //if dead -> ignore input
        {
            if (Input.GetKeyDown(KeyCode.Space)) //GetKeyDown = The moment you press Space, GetKey = as long as its pressed, GetKeyUp = when the key is released
            {
                rbody.velocity = Vector3.zero; //Reset current velocity to start always at the same level of acceleration
                rbody.AddForce(new Vector2(0, upForce)); //Accelerates upwards
            }
        }
	}

    private void OnCollisionEnter2D(Collision2D collision) //colliding results in dying
    {
        isDead = true;
    }
}
