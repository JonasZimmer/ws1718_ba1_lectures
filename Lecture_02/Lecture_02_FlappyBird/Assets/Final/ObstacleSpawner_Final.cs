﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// OBSTACLE SPAWNING MANAGER
/// This class will spawn the Obstacles after a giving time
/// 
/// BA1 Game Programming - Jonas Zimmer
/// Recreate FLAPPY BIRD, the addictive surprising hit from 2013
/// </summary>
public class ObstacleSpawner_Final : MonoBehaviour
{
    public GameObject obstaclePrefab;
    public float timeBetweenObstacles;
    private float timeSinceLastObstacle;

    /// <summary>
    /// UNITY SYSTEM METHOD
    /// Start by creating a new obstacle
    /// </summary>
    void Start ()
    {
        CreateObstacle();
    }

    /// <summary>
    /// UNITY SYSTEM METHOD
    /// Checks wether it's time to create a new obstacle
    /// </summary>
    void Update ()
    {
        timeSinceLastObstacle += Time.deltaTime;
        if (timeSinceLastObstacle >= timeBetweenObstacles)
            CreateObstacle();
	}

    /// <summary>
    /// Creates a new obstacle at a giving position.
    /// Resets the timer to 0.
    /// </summary>
    private void CreateObstacle()
    {
        RectTransform newObstacle = (RectTransform) (GameObject.Instantiate(obstaclePrefab, transform).transform);
        newObstacle.anchoredPosition = new Vector3(550, Random.Range(-150f, 150f));
        timeSinceLastObstacle = 0;
    }
}
