﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// OBSTACLE BEHAVIOUR CLASS
/// This class manages the obstacle movement and destroying 
/// them after they get out of view.
/// 
/// BA1 Game Programming - Jonas Zimmer
/// Recreate FLAPPY BIRD, the addictive surprising hit from 2013
/// </summary>
public class Obstacle_Final : MonoBehaviour
{
    private RectTransform rect; //reference to the RectTransform. Needs "using UnityEngine.UI;"
    public float speed; //How fast should the obstacles move. 

    /// <summary>
    /// UNITY SYSTEM METHOD
    /// Start is called at the creation of the class
    /// Gets RectTransform component
    /// </summary>
    void Start ()
    {
        rect = gameObject.GetComponent<RectTransform>();
	}

    /// <summary>
    /// UNITY SYSTEM METHOD
    /// Updates the gameobjects position each frame
    /// </summary>
    void Update ()
    {
        rect.anchoredPosition += new Vector2(-speed * Time.deltaTime, 0);

        //if the object is out of view, we destroy it
        if (rect.anchoredPosition.x < -550)
            Destroy(gameObject);
	}
}
