﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExampleScript : MonoBehaviour
{
    public Light lightBulb;
    private bool switchState = false;
    public int resultOfCalculation = 0;

    // Use this for initialization
    void Start ()
    {
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown("space"))
        {
            LightSwitch();
            resultOfCalculation = AddTwoNumbers(2, resultOfCalculation);
            Debug.Log("Result: " + resultOfCalculation);
        }

        lightBulb.enabled = switchState;
	}

    void LightSwitch()
    {
        if (switchState == false)
        {
            switchState = true;
        }
        else
        {
            switchState = false;
        }
    }

    //returns the value of var1 + var2
    int AddTwoNumbers(int var1, int var2)
    {
        int returnValue = var1 + var2;
        return returnValue;
    }
}
