﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// GAME MANAGER CLASS
/// BA1 Game Programming - Jonas Zimmer
/// Recreate PONG, the mother of all video games
/// </summary>
public class GameManager_Final : MonoBehaviour
{ 

    public Ball_Final ball; //reference to the ball
    public Transform playerPaddle, enemyPaddle; //reference to the paddles

    public Text PlayerScore, EnemyScore; //Textcomponent to display the scores
    private float enemyScore, playerScore; //Variables to store the player- and enemyscore    

    public KeyCode moveUp, moveDown, shootBall; //Control scheme for player paddle
    public float playerPaddleMaxPosition, playerPaddleMaxSpeed, playerPaddleAcceleration; //player paddle parameters
    private float playerPaddleSpeed; //current speed of the playerPaddle

    public float enemyPaddleFrequence; //how fast does the enemy paddle move
    public float enemyPaddleAmplitude; //how far does the enemy paddle move

    private float elapsedTime; //time since start of the game
    private bool playerPaddleHasBall; //boolean to tell if the ball is currently attached to the playerPaddle


    /// <summary>
    /// UNITY SYSTEM METHOD
    /// Start() gets called at the beginning of the program
    /// </summary>
    void Start()
    {
        elapsedTime = 0;
        enemyScore  = 0;
        playerScore = 0;

        GrabBall();
    }

    /// <summary>
    /// UNITY SYSTEM METHOD
    /// Update() gets called once a frame - usually around 30 - 60 times per second 
    /// </summary>
    void Update()
    {
        // ---------- Update the UI ----------

        PlayerScore.text = "PLAYER: " + playerScore.ToString();
        EnemyScore.text = "ENEMY: " + enemyScore.ToString();


        // ---------- Control the Player Padle ----------

        //Checks wether the Buttons were pressed and raises or lowers the playerPaddleSpeed
        if (Input.GetKey(moveUp))
            playerPaddleSpeed += playerPaddleAcceleration;
        if (Input.GetKey(moveDown))
            playerPaddleSpeed -= playerPaddleAcceleration;

        //Decelerate the paddles speed each frame
        playerPaddleSpeed *= 0.9f;

        //Make sure the speed is between -playerPaddleMaxSpeed and playerPaddleMaxSpeed
        playerPaddleSpeed = 
            Mathf.Clamp(playerPaddleSpeed, -playerPaddleMaxSpeed, playerPaddleMaxSpeed); 

        //Calculate the new position, make sure it is not higher than playerPaddleMaxPosition or lower than -playerPaddleMaxPosition
        playerPaddle.position = new Vector3(
            playerPaddle.position.x,
            Mathf.Clamp(playerPaddle.position.y + (playerPaddleSpeed * Time.deltaTime), -playerPaddleMaxPosition, playerPaddleMaxPosition),
            playerPaddle.position.z);
        //If the paddle currently holds the ball, change the balls position based on the paddles position, too.
        if (playerPaddleHasBall == true)
            ball.transform.position = playerPaddle.position + new Vector3(0.8f, 0, 0);

        //If the paddle holds the ball and the shootBall command gets pressed: Shoot
        if (Input.GetKey(shootBall) && playerPaddleHasBall == true)
        {
            ball.Push(new Vector3(15, Random.Range(10f, 15f), 0));
            playerPaddleHasBall = false;
        }


        // ---------- Control the enemy ----------

        //Add elapsed time to our time variable
        elapsedTime += Time.deltaTime;

        //Adjust the position based on a sinus wave
        enemyPaddle.position = new Vector3(
                                enemyPaddle.position.x,
                                Mathf.Sin(elapsedTime * enemyPaddleFrequence) * enemyPaddleAmplitude,
                                enemyPaddle.position.z);
    }

    /// <summary>  
    /// Increases the enemy score and grabs the ball
    /// </summary>
    public void EnemyScored()
    {
        enemyScore++;
        GrabBall();
    }
    /// <summary>
    /// Increases the player score and grabs the ball
    /// </summary>
    public void PlayerScored()
    {
        playerScore++;
        GrabBall();
    }

    /// <summary>
    /// Resets the balls position in front of the player paddle.
    /// Sets the balls velocity to zero (ball.Stop())
    /// </summary>
    public void GrabBall()
    {
        playerPaddleHasBall = true;
        ball.Stop();
        ball.transform.position = playerPaddle.position + new Vector3(0.8f, 0, 0);
    }
}
