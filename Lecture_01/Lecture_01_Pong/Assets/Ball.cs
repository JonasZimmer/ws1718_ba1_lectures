﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public Rigidbody rbody; //reference to the gameobjects rigidbody
    public GameManager gameManager; //reference to the game manager class
    public float collisionAcceleration, maxSpeed; //movement parameters

    /// <summary>
    /// Stops the balls velocity
    /// </summary>
    public void Stop()
    {
        rbody.velocity = Vector3.zero;
    }

    /// <summary>
    /// Pushes the ball away from the paddle
    /// </summary>
    /// <param name="force">The given force to be pushed</param>
    public void Push(Vector3 force)
    {
        rbody.AddForce(force);
        rbody.AddTorque(new Vector3(Random.Range(-20f, 20f), Random.Range(-20f, 20f), Random.Range(-20f, 20f)));
    }

    /// <summary>
    /// UNITY SYSTEM METHOD
    /// This method gets called when this gameobject collides with another gameobject.
    /// The collider component of this gameobject needs to be set to "IsTrigger".
    /// The method will increase the balls speed every time it collides with something.
    /// It will also mirror the horizontal velocity (velocity.x) if it hits a vertical wall
    /// and the vertical velocity (velocity.y) if it hits a horizontal wall.
    /// If it collides with one of the goals, it calls the ccorresponding method in gamemanager.
    /// </summary>
    /// <param name="other">The collider of the other gameobject</param>
    private void OnTriggerEnter(Collider other)
    {        
        if (other.gameObject.tag == "colVertical")
        {
            //Speed up velocity and invert horizontal velocity

        }
        else if (other.gameObject.tag == "colHorizontal")
        {
            //Speed up velocity and invert vertical velocity

        }
        else if (other.gameObject.tag == "colGoal")
        {
            //Score a point
        }
    }
}
