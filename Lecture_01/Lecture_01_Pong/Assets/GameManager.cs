﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// GAME MANAGER CLASS
/// BA1 Game Programming - Jonas Zimmer
/// Recreate PONG, the mother of all video games
/// </summary>
public class GameManager : MonoBehaviour
{ 
    public Ball ball; //reference to the ball
    public Transform playerPaddle, enemyPaddle; //reference to the paddles
    
    private float enemyScore, playerScore; //Variables to store the player- and enemyscore    


    /// <summary>
    /// UNITY SYSTEM METHOD
    /// Start() gets called at the beginning of the program
    /// </summary>
    void Start()
    {
        //Reset values and set up start position
    }

    /// <summary>
    /// UNITY SYSTEM METHOD
    /// Update() gets called once a frame - usually around 30 - 60 times per second 
    /// </summary>
    void Update()
    {
        // ---------- Control the Player Padle ----------                

        // ---------- Control the enemy ----------

        // ---------- Update the UI ----------

    }

    /// <summary>  
    /// Increases the enemy score and grabs the ball
    /// </summary>
    public void EnemyScored()
    {

    }
    /// <summary>
    /// Increases the player score and grabs the ball
    /// </summary>
    public void PlayerScored()
    {

    }

    /// <summary>
    /// Resets the balls position in front of the player paddle.
    /// Sets the balls velocity to zero (ball.Stop())
    /// </summary>
    public void GrabBall()
    {

    }
}
